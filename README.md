# Anow Ping Pong API

This is a simple API that returns ping-pong leaderboard scores.

## Endpoints / Websockets

The following endpoints and websockets are defined:

-   `GET http://localhost:3000/leaderboard` - returns the current leaderboard scores.
-   `ws://localhost:3000/leaderboard` - returns the current leaderboard scores upon
    connect and resends the leaderboard when something changes.

There is an internal process that occasionally updates the leaderboard, thereby
mimicking a real API that has occasional updates.  Please see the `Environment
Variables` section below for information on how to control this.

## Running

To get going, clone this repo, then run:

```sh
yarn
```

to grab all of the dependencies.

To start the API, run:

```sh
yarn start
```

# Environment Variables

This server can be slightly configured by setting the following environment
variables:

-   `PORT` - the port number the server runs on (default: 3000)
-   `MIN_REFRESH` - the minimum number of seconds between updates to the
                    leaderboard (default: 10)
-   `MAX_REFRESH` - the maximum number of seconds between updates to the
                    leaderboard (default: 60)

## Hacking

Although this is a quick and dirty application, it is relatively representative of a typical
node project at Anow.  Specifically:

-   the use of [`.editorconfig`](http://editorconfig.org/) to control basic editor settings
-   the use of [`yarn`](https://yarnpkg.com) over `npm` to manage dependencies and run scripts
-   the use of scripts in `package.json`, implemented in a cross-platform way, to perform common developer tasks
-   the use of [`eslint`](https://eslint.org/), with the stock Airbnb rulelist to lint javascript and json
-   additional linters for other components
-   the use of `.nvmrc` to determine the version of node required, so that we can use [NVM](https://github.com/creationix/nvm) to manage our node versions
-   the use of [Babel](https://babeljs.io), so we can use the latest features of JS without needing to wait for Node or Browsers to catch up.
-   the use of [pre-commit]() to perform sanity checks whilst committing to git.
